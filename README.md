# Metric-based design defect dataset


## Description
This dataset provides model-based static design metrics derived from the UnifiedBugDataset 1.2 (http://www.inf.u-szeged.hu/~ferenc/papers/UnifiedBugDataSet/), which combines and extends several widely used public bug datasets, including:

* PROMISE
* Eclipse Bug Dataset
* Bug Prediction Dataset
* Bugcatchers Bug Dataset
* GitHub Bug Dataset
* Dataset Construction



## Usage
To construct the model-based metrics datasets from source code, the following steps were performed:
* Reverse Engineering: We reverse-engineered open-source projects to extract structural design models, specifically UML class diagrams and package diagrams, which were then stored as META models in XMI format.
* Metric Calculation: Using the SDmetrics2 tool, we calculated static design metrics on the     extracted models.
* Bug Mapping: Finally, we mapped the design metrics to corresponding bug counts.


## Authors and acknowledgment
Batnyam Battulga, Lkhamrolom Tsoodol, Enkhzol Dovdon, Naranchimeg Bold, Oyun-Erdene Namsrai

Department of Information and Computer Science, National University of Mongolia

## License
Open source project, it is not licensed.

## Project status
In Research.
Old version of Dataset is changed to new version "design metrics data version 1.0".

All directories are archived and pushed in a directory "all files archived".

A large XMI-file is compressed in some folders. For example: Eclipse JDT Core 3.1.rar file in Bugcatchers/xmi folder, Eclipse-2.1.rar and Eclipse-3.0.rar files in Zimmerman/xmi folder